# Projeto AC4 - Biscoito da Sorte

Este é um projeto web para gerar mensagens motivacionais no estilo "biscoito da sorte", utilizando o framework Flask e Docker para facilitar a portabilidade e implantação da aplicação.
O GitLab CI/CD é usado para automatizar o processo de integração e implantação contínua.
O projeto inclui testes automatizados e segue as boas práticas de programação, como o padrão PEP 8.

## Testes

Para executar o teste unitário, é necessário ter o Python instalado na máquina e também a biblioteca unittest (que já é uma biblioteca padrão do Python).  
 Passo a passo para executar o teste:

1. Clone o repositório para sua máquina:

```
git clone https://gitlab.com/diego.brito/projeto-ac4-biscoito-da-sorte.git
```

2. Navegue para o diretório do projeto:

```
cd biscoito-da-sorte
```

3. Execute o teste com o seguinte comando:

```
python -m unittest testeunitario_app.py
```

O resultado do teste será exibido no console.

## Como usar

Para executar a aplicação, é necessário ter o Docker instalado. Após clonar o repositório, abra um terminal na pasta do projeto e execute os comandos abaixo:

```bash
  docker build -t ac4biscoitodasorte .
```

```bash
  docker run -p 5000:5000 ac4biscoitodasorte
```

Isso irá construir a imagem do Docker e executar a aplicação.
Acesse o endereço abaixo para ver a página inicial.

[http://localhost:5000](http://localhost:5000)

## CI/CD

Integração contínua (CI) - Devs fazem check-in de seu código em um repositório centralizado com frequência. A cada check-in, uma série de testes é executada automaticamente para garantir que o novo código não cause problemas no código existente.

Implantação contínua (CD)- Extensão da integração contínua, em que a cada atualização do código-fonte no repositório é construída, testada e, em seguida, implantada automaticamente em um ambiente de produção ou de teste.

# Autores

RA: 2200910 | Cleiton Jesus Barros Junior  
RA: 2203193 | Diego Rafael Brito Alves  
RA: 2201130 | Gustavo Soares de Souza  
RA: 2201402 | Maxsuel de Jesus Quaresma
