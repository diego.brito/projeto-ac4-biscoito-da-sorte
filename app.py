import random
from flask import Flask, render_template, request


class Desenvolvedor:
    def __init__(self, nome, matricula):
        self.nome = nome
        self.matricula = matricula


d1 = Desenvolvedor('2200910', 'Cleiton Jesus Barros Junior')
d2 = Desenvolvedor('2203193', 'Diego Rafael Brito Alves')
d3 = Desenvolvedor('2201130', 'Gustavo Soares de Souza')
d4 = Desenvolvedor('2201402', 'Maxsuel de Jesus Quaresma')
listaDev = [d1, d2, d3, d4]


app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def index():
    cookieAberto = 'static/fortunecookieopen.png'
    cookieFechado = 'static/fortunecookie.png'
    if request.method == "GET":
        return render_template('index.html', desenvolvedores=listaDev,
                               cookie=cookieAberto)
    elif request.method == "POST":
        frases = ler_txt()
        frase_aleatoria = random.choice(frases)
        return render_template('index.html', desenvolvedores=listaDev,
                               cookie=cookieFechado, frase=frase_aleatoria)


def ler_txt():
    array_linhas = []
    with open('static/frases.txt', 'r', encoding='utf-8') as arquivo:
        for linha in arquivo:
            array_linhas.append(linha.strip())
    return array_linhas


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)


def somar(a, b):
    return a + b
