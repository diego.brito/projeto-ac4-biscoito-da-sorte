FROM python:3.11.2-slim-buster
WORKDIR /ac4BiscoitoDaSorte
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . .
EXPOSE 5000
CMD ["flask", "run", "--host=0.0.0.0"]
