import unittest
from app import somar


class TestSomar(unittest.TestCase):

    def test_soma(self):
        self.assertEqual(somar(2, 3), 5)
        self.assertEqual(somar(-1, 1), 0)
        self.assertEqual(somar(0, 0), 0)
        self.assertEqual(somar(0, 3), 3)
